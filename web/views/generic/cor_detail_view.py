from .detail_view import DetailView


class CorDetailView(DetailView):
    template_name_suffix = "_cor-detail"
