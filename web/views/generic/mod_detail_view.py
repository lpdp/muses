from .detail_view import DetailView


class ModDetailView(DetailView):
    template_name_suffix = "_mod-detail"
