from .update_view import UpdateView


class CorUpdateView(UpdateView):
    template_name_suffix = "_cor-update"
