from .delete_view import DeleteView


class ModDeleteView(DeleteView):
    template_name_suffix = "_mod-delete"
