from .update_view import UpdateView


class AdminUpdateView(UpdateView):
    template_name_suffix = "_admin-update"


