from .update_view import UpdateView


class ModUpdateView(UpdateView):
    template_name_suffix = "_mod-update"
