from .create_view import CreateView


class CorCreateView(CreateView):
    template_name_suffix = "_cor-create"
