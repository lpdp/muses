from .delete_view import DeleteView


class CorDeleteView(DeleteView):
    template_name_suffix = "_cor-delete"
