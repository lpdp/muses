from .create_view import CreateView


class ModCreateView(CreateView):
    template_name_suffix = "_mod-create"
