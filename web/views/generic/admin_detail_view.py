from .detail_view import DetailView


class AdminDetailView(DetailView):
    template_name_suffix = "_admin-detail"

