from http.server import SimpleHTTPRequestHandler
from socketserver import TCPServer


class CORSRequestHandler(SimpleHTTPRequestHandler):
    def end_headers(self):
        self.send_header('Access-Control-Allow-Origin', '*')
        SimpleHTTPRequestHandler.end_headers(self)


PORT = 8001
Handler = CORSRequestHandler
httpd = TCPServer(("", PORT), Handler)
print("serving at port", PORT)
httpd.serve_forever()
