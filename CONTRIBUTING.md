# Comment contribuer ?

Toute contribution doit être approuvée pour être intégrée et déployée sur le site.
Pour cela, il est impératif de passer par le processus de Pull Request (Demandes de fusion) de Gitlab.

Vous trouverez ci-dessous les ressources d'aide nécessaires pour apprendre comment faire.

* [Cloner le dépôt de Muses](https://gitlab.com/lpdp/muses/-/wikis/Cloner-le-d%C3%A9p%C3%B4t-de-Muses)
* [Créer un ticket](https://gitlab.com/lpdp/muses/-/wikis/Cr%C3%A9er-un-ticket)
* Sélectionner la branche de son ticket

## Proposer un nouveau thème

Commencez par consulter la section du Wiki consacrée à ce thème pour apprendre 
le fonctionnement des thèmes. Vous saurez ensuite les éléments à fournir dans le cycle.

* [Création d'un thème](https://gitlab.com/lpdp/muses/-/wikis/Cr%C3%A9ation-d'un-th%C3%A8me)

## Proposer une nouvelle fonctionnalité

TBD