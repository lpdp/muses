[![Codeship Status for lpdp/muses](https://app.codeship.com/projects/ce4ac000-43ba-0138-b625-5e70c3be625d/status?branch=master)](https://app.codeship.com/projects/388215)

# Roadmap du projet

Vous pouvez consulter la Roadmap du projet à l'adresse suivante: [https://gitlab.com/lpdp/muses/-/milestones](https://gitlab.com/lpdp/muses/-/milestones)

# Installation

Merci de lire le fichier suivant [INSTALL.md](INSTALL.md)


# Contribuer au développement de Muses

Merci de lire le fichier suivant [CONTRIBUTING.md](CONTRIBUTING.md)


# Contact

Pour toute demande, envoyez un email à [devteam@lapassiondespoemes.com](mailto://devteam@lapassiondespoemes.com)