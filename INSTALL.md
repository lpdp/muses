# Nécessaire de développement

Pour contribuer au développement de LPDP, vous aurez besoin d'installer sur 
votre poste un certain nombre de logiciels. Tous les outils nécessaires sont 
disponibles en Open Source. 
Du côté de l'IDE, vous pouvez utiliser Visual Studio Code disponible 
gratuitement. Vous pouvez aussi utiliser aussi la version Community de PyCharm.
Pour être totalement confortable, aussi investir dans la version Pro de PyCharm
qui rajoute des fonctionnalités pratiques pour le framework Django.

## Liste des logiciels

### Langage de développement

* Python Python 3.7.5: https://www.python.org/downloads/release/python-375/

### IDE

* Visual Studio Code: https://code.visualstudio.com/download
* PyCharm: https://www.jetbrains.com/pycharm/download/

# Installation de l'environnemnt de développement

Clonage du dépôt Git
```console
git clone https://gitlab.com/lpdp/muses.git
```
Déplacement dans le dossier du dépôt local
```console
cd muses
```
Création de l'environnement virtuel
```console
python -m venv env
```
Sous Linux:
```console
source env/bin/activate
```
Sous Windows en ligne de commande:
```console
env\Scripts\activate
```
Sous Windows en shell Powershell:
```console
.\env\Scripts\activate.ps1
```
Installation des packages Python prérequis
```console
pip install -r requirements.txt
```




