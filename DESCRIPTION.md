## Roadmap du projet

Vous pouvez consulter la Roadmap du projet à l'adresse suivante: https://gitlab.com/lpdp/muses/-/milestones

## Contact

Pour toute demande, envoyez un email à devteam@lapassiondespoemes.com