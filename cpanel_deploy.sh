declare -A ENVIRONMENT_MAP=( [development]=DEV [staging]=STG [master]=PRD )
echo -e "Muses deployment"
git_url=$(git config --get remote.origin.url)
branch=$(git symbolic-ref HEAD | sed -e 's,.*/\(.*\),\1,')
environment=${ENVIRONMENT_MAP[$branch]}
echo -e "git url::$git_url, branch::$branch, environment::$environment"